This library defines a PHP API for the [Digital Measures](http://www.digitalmeasures.com/) service used to store faculty data. It fetches DM API responses, which are in XML, parses them into [PHP SimpleXMLElement objects](http://php.net/manual/en/class.simplexmlelement.php), and caches them.


## Installation
Create a [composer.json](https://getcomposer.org/doc/00-intro.md) file that searches the VCS repository at https://gitlab.msu.edu/canr/DigitalMeasures.git and requires the package "canr/DigitalMeasures".

    {
        "repositories": [
            {
                "type": "vcs",
                "url": "https://gitlab.msu.edu/canr/DigitalMeasures.git"
            }
        ],
        "require": {
            "canr/DigitalMeasures": "*"
        }
    }

Then run the Composer install command.

```
composer install
```


## Configuration
Make a copy of the distribution configuration file.

```
cp vendor/canr/DigitalMeasures/config.ini.dist vendor/canr/DigitalMeasures/config.ini
```

Edit the file to contain your personal Digital Measures credentials.

    [authentication]
    login = 'canr/slenkeri'
    password = 'mypassword'


## Usage
Require the Composer autoloader in your application's vendor directory and instantiate a Digital_measures_api object.

```php
<?php
// Load and use DM library
require realpath($your_app_root . '/vendor/autoload.php');
use \DigitalMeasures\Digital_measures_api;

// Create API object
$dm = new Digital_measures_api();
```

Once you've got the Digital_measures_api object you can use it to select the target user and query their data.

```php
// Set the user whose data is to be queried
$dm->set_user('campa');

// Get the user's intellectual content data
$publications = $dm->get('//INTELLCONT');
```

Data is returned as a [SimpleXMLElement](http://php.net/manual/en/class.simplexmlelement.php).

```php
// Get the first publication's title
echo "First publication: \"{$publications[0]->TITLE}\"\n";

// Get each publication's title and authors
foreach ($publications as $publication)
{
    $author_names = array();
    foreach ($publication->INTELLCONT_AUTH as $author)
    {
        $author_names[] = "{$author->FNAME} {$author->MNAME} {$author->LNAME}";
    }
    echo "\"{$publication->TITLE}\" by " . implode(', ', $author_names) . ".\n";
}
?>
```

For more examples, see the scripts in the examples directory.


## Public Client Methods

### __construct($config_file_path)
Creates the DM API instance with the configuration specified in $config_file_path. Default configuration file path is './config.ini'.

### set_user($user)
Sets the user whose data is to be fetched to $user. If the username is invalid, no user is set.

### get_user()
Gets the name of the user whose data is to be fetched.

### get($xpath)
Gets an XML entity by its XPath. See the Entities section below for a list of all valid entities and their descriptions. An entity's XPath is best written as "//$entity_name". If no user is set, returns an empty string. Throws an Exception if the XPath is invalid. Returns a SimpleXMLElement containing the target element.

### get_entities_list()
Gets the names of the XML entities available for the set user. Returns an array of strings.


## Entities
The list below is an incomplete list of the entities which MSU maintains for a given user. This list is currently subject to change, and includes entities which are no longer available for historic purposes.

* PCI - Personal and Contact Information
* ADMIN_PERM - Permanent Data
* ADMIN - Yearly Data
* PASTHIST - Non-MSU Professional Positions
* EDUCATION - Education
* FACDEV - Faculty Development Activities Attended
* LICCERT - Licensures and Certifications
* MEDCONT - Media Appearances
* WORKLOAD - Work Load Information
* ACADVISE - Academic Advising
* DSL - Graduate Committees
* NCTEACH - Non-Credit Instruction Taught
* SCHTEACH - Scheduled Teaaching
* SCHTEACH_OTHER - Other Scheduled Teaching
* MENTORING - Mentoring
* NIH_BIOSKETCH - NIH Biographical Sketch
* CONGRANT - Sponsored Program Proposals
* CONGRANT_OTHER - Other Programs/ Projects and Non-MSU Administered Sponsored Program Proposals **(Deprecated)**
* AWARDHONOR - Awards and Honors
* INTELLCONT - Publications, Papers, and Other Creative Works
* INTELLPROP - Intellectual Property (e.g., copyrights, patents)
* INTELLPROP_EXTERNAL - Intellectual Property (e.g., copyrights, patents) (external to MSU)
* PRESENT - Presentations
* SERVICE_PROFESSIONAL - Service to Scholarly and Professional Organizations (external to MSU) **(Deprecated)**
* SERVICE_UNIVERSITY - Service Within the University **(Deprecated)**
* SERVICE_PUBLIC - Service Within the Broader Community **(Deprecated)**
* GENSERVE - Service
* IMPACT - Impact Summaries
* EXTENSION - MSU Extension Activities