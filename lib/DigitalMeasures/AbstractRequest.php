<?php
namespace DigitalMeasures;

use \Curl\Curl;
use \DigitalMeasures\iRequest;

/**
 * Abstract class for a Digital Measures Request.
 */
abstract class AbstractRequest implements iRequest
{
    /**
     * cURL class object.
     * 
     * @var Curl
     */
    public $curl = NULL;

    /**
     * The Digital Measures API object which holds the cache.
     *
     * @var Digital_measures_api
     */
    protected $_api = NULL;

    /**
     * The cURL response object.
     * 
     * @var SimpleXMLElement
     */
    protected $_response = NULL;

    /**
     * URL to be requested.
     *
     * @var string
     */
    protected $_url = '';

    /**
     * Request identification string to be used as a key in the cache.
     *
     * @var string
     */
    protected $_id = '';


    /**
     * Constructor.
     * 
     * @param Digital_measures_api $api The Digital Measures API object which holds the cache.
     */
    public function __construct ($api)
    {
        $this->_api = $api;
        $this->curl = new Curl();
    }

    /**
     * Sets the URL to be requested.
     *
     * @param string $url The URL to be requested.
     */
    public function setUrl ($url)
    {
        $this->_url = $url;
    }

    /**
     * Gets the Request's identification string.
     *
     * @return string The string used to identify the Request in the cache.
     */
    public function getId ()
    {
        return $this->_id;
    }

    /**
     * Gets the request's response.
     * 
     * @return SimpleXMLElement The request's response.
     */
    public function getResponse ()
    {
        return $this->_response;
    }

    /**
     * Gets the request's HTTP status code.
     * 
     * @return int The request's HTTP status code.
     *
     * @throws Exception If the three digit status code is not found in the status line.
     * @throws Exception If there is an error parsing the status line.
     */
    public function getStatus ()
    {
        $matches = array();
        $httpStatusCodeFound = preg_match('/HTTPS?\/\d\.\d (\d{3}) .*/', $this->curl->responseHeaders['Status-Line'], $matches);

        if ( $httpStatusCodeFound === 0 )
        {
            throw new \Exception('Unable to find HTTP status code in header status line.');
        }
        else if ( $httpStatusCodeFound === FALSE )
        {
            throw new \Exception('Error parsing status code from header status line.');
        }

        return (int)$matches[1];
    }


    /**
     * Handles cURL errors.
     * 
     * @throws Exception If cURL throws an error.
     * @throws Exception If Digital Measures responds with an error message.
     */
    protected function _handleErrors ()
    {
        if ( $this->curl->error )
        {
            throw new \Exception('Digital Measures: CURL Error #'.$this->curl->errorCode.': '.$this->curl->errorMessage);
        }
        if ( $this->curl->response && $this->curl->response->Message )
        {
            throw new \Exception('Digital Measures: Error: '.$this->curl->response->Message);
        }
    }
}
?>