<?php
namespace DigitalMeasures;

use \DigitalMeasures\Request;


/**
 * Factory for Digital Measures entity requests.
 *
 * @author Eric Slenk <slenkeri@anr.msu.edu>
 */
class RequestFactory
{
    /**
     * @const ENTITY_LIST_URL URL to the API endpoint that returns a view of the common entites in the system.
     */
    const ENTITY_LIST_URL = 'https://digitalmeasures.com/login/service/v4/SchemaEntity/INDIVIDUAL-ACTIVITIES-University/';

    /**
     * @const RECORDS_BASE_URL URL base for endpoints that return entity records.
     */
    const RECORDS_BASE_URL = 'https://digitalmeasures.com/login/service/v4/SchemaData/INDIVIDUAL-ACTIVITIES-University/USERNAME:';

    /**
     * The Digital Measures API object which holds this class.
     *
     * @var Digital_measures_api
     */
    private $_api = NULL;

    /**
     * The user whose data is to be requested.
     *
     * @var string
     */
    private $_user = '';


    /**
     * Constructor.
     * 
     * @param Digital_measures_api $api The Digital Measures API object which holds this class.
     */
    public function __construct ($api)
    {
        $this->_api = $api;
    }

    /**
     * Gets the username of the user whose data is to be queried.
     * 
     * @return string Username of the user whose data is to be queried.
     */
    public function getUser ()
    {
        return $this->_user;
    }

    /**
     * Sets the user whose data is to be requested.
     * 
     * @param string $username Name of the user whose data is to be requested.
     */
    public function setUser ($username)
    {
        $this->_user = $username;
    }

    /**
     * Creates a Digital Measures entity request.
     * 
     * @param  string $xpath XPath expression.
     *
     * @return Request Digital Measures request object.
     */
    public function createRecordRequest ($xpath)
    {
        $request = $this->_createRequest();
        $request->setXpath($xpath);
        $request->setUrl( RequestFactory::RECORDS_BASE_URL . $this->_user . '/' . $this->_getEntityName($xpath) );
        return $request;
    }

    /**
     * Creates a Digital Measures entity list request.
     * 
     * @param  string $xpath XPath expression.
     *
     * @return Request Digital Measures request object.
     */
    public function createEntityListRequest ()
    {
        $request = $this->_createRequest();
        $request->setUrl( RequestFactory::ENTITY_LIST_URL );
        return $request;
    }


    /**
     * Determines whether a given username is valid.
     *
     * @param string $user Username to be validated.
     *
     * @return boolean      Whether or not the given username is valid.
     */
    private function _is_valid_user ($user)
    {
        return ! ( (bool)preg_match('/[\{\}]/', $user) );
    }

    /**
     * Creates a request object and gives it the default configuration.
     *
     * @return Request A Digital Measures request object.
     */
    private function _createRequest ()
    {
        $request = new Request($this->_api);

        $request->curl->setBasicAuthentication($this->_api->credentials->getLogin(), $this->_api->credentials->getPassword());
        $request->curl->setOpt(CURLOPT_ENCODING, '');
        $request->curl->setOpt(CURLOPT_FOLLOWLOCATION, TRUE);
        $request->curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
        $request->curl->setOpt(CURLOPT_SSLVERSION, 1);
        $request->curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);

        return $request;
    }

    /**
     * Parses the name of the entity being requested from the given XPath.
     * 
     * @param  string $xpath The XPath for the entity being requested.
     * @return string The name of the entity.
     */
    private function _getEntityName ($xpath)
    {
        $matches = array();
        preg_match('/\/\/(\w+)/', $xpath, $matches);

        return $matches[1];
    }

}
?>