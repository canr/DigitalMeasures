<?php
namespace DigitalMeasures;

use \DigitalMeasures\Credentials;
use \DigitalMeasures\ConfigurationManager;
use \DigitalMeasures\RequestFactory;


/**
 * Class to query the Digital Measures API.
 *
 * @author   Eric Slenk <slenkeri@anr.msu.edu>
 */
class Digital_measures_api
{
	/**
	 * Set of Digital Measures credentials used to authenticate.
	 *
	 * @var Credentials
	 */
	public $credentials = NULL;

	/**
	 * Cache for Digital Measures API response objects.
	 * 
	 * @var array Array of SimpleXMLElement objects representing the XML documents sent in response to API calls. Indecies represent the name of the entity queried.
	 */
	public $cache = array();

	/**
	 * Configuration file manager.
	 *
	 * @var  ConfigurationManager
	 */
	public $config = NULL;

	/**
	 * Digital Measures data request object factory.
	 *
	 * @var RequestFactory
	 */
	private $_requestFactory = NULL;

	

	/**
	 * Constructor.
	 *
	 * @param string $config_file_path Path to the API configuration file.
	 *
	 * @throws Exception If no configuration file is specified and no configuration file is found in the default location.
	 */
	public function __construct($config_file_path = '')
	{
		// Objects
		$this->credentials = new Credentials();
		$this->config = new ConfigurationManager($this->credentials);
		$this->_requestFactory = new RequestFactory($this);

		// Load config
		if ( $config_file_path === '' )
		{
			if ( realpath(dirname(__FILE__).'/../../config.ini') )
			{
				$config_file_path = realpath( dirname(__FILE__).'/../../config.ini' );
			}
			else
			{
				throw new \Exception ('No configuration file found.');
			}
		}
		$this->config->loadConfigFile( $config_file_path );
	}

	/**
	 * Gets the username of the user whose data is to be queried.
	 * 
	 * @return string Username of the user whose data is to be queried.
	 */
	public function get_user ()
	{
		return $this->_requestFactory->getUser();
	}

	/**
	 * Sets the username of the user whose data is to be queried.
	 *
	 * @param string $user Username for the user whose data is to be queried.
	 *
	 * @return void 
	 */
	public function set_user ($user)
	{
		$this->_requestFactory->setUser($user);
	}


	/**
	 * Gets some XML element from the API.
	 * 
	 * @param string $xpath An XPath query string which selects the desired element(s).
	 *
	 * @return SimpleXMLElement Returns the value of the element.
	 *
	 * @throws Exception If given XPath is invalid or throws an error.
	 */
	public function get ($xpath)
	{
		// Get data
		$request = $this->_requestFactory->createRecordRequest($xpath);
		$request->send();

		return $request->getResponse();
	}

	/**
	 * Gets a list of API Entities available.
	 * 
	 * @return array List of Entity keys.
	 */
	public function get_entities_list ()
	{
		// Get the list
		$request = $this->_requestFactory->createEntityListRequest();
		$request->send();
		$record = $request->getResponse();

		// Convert to list
		$entities = array();
		foreach ($record->View->Entity as $entity)
		{
			$entities[] = (string)$entity->attributes()->key;
		}

		return $entities;
	}

}
