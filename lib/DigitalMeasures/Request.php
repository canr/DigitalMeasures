<?php
namespace DigitalMeasures;

use \DigitalMeasures\AbstractRequest;

/**
 * A Digital Measures entity request.
 *
 * @author  Eric Slenk <slenkeri@anr.msu.edu>
 */
class Request extends AbstractRequest
{
    /**
     * XPath of the entity requested.
     *
     * @var  string
     */
    private $_xpath = '';

    /**
     * Name of the entity requested. XPath if available, otherwise URL.
     *
     * @var string
     */
    private $_name = '';


    /**
     * Sets the XPath of the element to be requested.
     * 
     * @param string $xpath XPath of the element to be requested.
     */
    public function setXpath ($xpath)
    {
        $this->_xpath = $xpath;
    }

    /**
     * Sends the cURL request and handles the response.
     */
    public function send ()
    {
        // Set name
        $this->_name = $this->_xpath ?: $this->_url;

        // Is entity cached?
        if ( isset($this->_api->cache[$this->getId()]) )
            return $this->_api->cache[$this->getId()];
        
        // Send cURL request
        $this->curl->get( $this->_url );

        // Handle response
        $this->_handleErrors();
        $this->_handleHttpStatus();

        // Parse response
        $this->curl->response = new \SimpleXMLElement( str_replace( 'xmlns=', 'ns=', $this->curl->response->asXML() ) );

        // Get element from tree
        $this->_response = $this->_xpath !== '' ?
            $this->curl->response->xpath($this->_xpath) :
            $this->curl->response;

        // Cache entity
        $this->_api->cache[$this->getId()] = $this->_response;
    }


    /**
     * Handles HTTP statuses.
     * 
     * @throws Exception If any HTTP status except 200 is set.
     */
    private function _handleHttpStatus ()
    {
        if ( $this->getStatus() !== 200 )
        {
            throw new \Exception('Digital Measures: HTTP Status '.$this->getStatus());
        }
    }

}
?>