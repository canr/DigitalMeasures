<?php
namespace DigitalMeasures;

/**
 * Set of credentials for the Digital Measures API.
 *
 * @author  Eric Slenk <slenkeri@anr.msu.edu>
 */
class Credentials
{
    /**
     * Login domain and name for DM authentication.
     * 
     * @var string
     */
    private $_login = '';

    /**
     * Password for DM authentication.
     *
     * @var string
     */
    private $_password = '';

    /**
     * Gets the login domain and name for DM authentication.
     * 
     * @return string Login domain and name for DM authentication.
     */
    public function getLogin ()
    {
        return $this->_login;
    }

    /**
     * Sets the login domain and name for DM authentication.
     *
     * @param  string $login Login domain and name used for DM authentication.
     *
     * @return void
     */
    public function setLogin ($login)
    {
        $this->_login = $login;
    }

    /**
     * Gets the password for DM authentication.
     *
     * @return string Password used for DM authentication.
     */
    public function getPassword ()
    {
        return $this->_password;
    }

    /**
     * Sets the password for DM authentication.
     *
     * @param  string  $password Password used for DM authentication.
     *
     * @return  void
     */
    public function setPassword ($password)
    {
        $this->_password = $password;
    }
}
?>