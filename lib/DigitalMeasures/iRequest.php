<?php
namespace DigitalMeasures;

/**
 * Interface to a Digital Measures Request.
 */
interface iRequest
{
    public function __construct ($api);
    public function setUrl ($url);
    public function getId ();
    public function send ();
    public function getResponse ();
    public function getStatus ();
}
?>