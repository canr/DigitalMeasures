<?php
namespace DigitalMeasures;

use \Exception;
use \InvalidArgumentException;

/**
 * Manages the system's configuration.
 *
 * @author  Eric Slenk <slenkeri@anr.msu.edu>
 */
class ConfigurationManager
{
    /**
     * Path to the loaded configuration file.
     * 
     * @var string
     */
    public $filePath = '';

    /**
     * The Digital Measures API object which holds this class.
     * 
     * @var Digital_measures_api
     */
    private $_credentials = NULL;

    /**
     * The array of configuration options parsed from the configuration file.
     *
     * @var array
     */
    private $_configuration = array();

    /**
     * 2D array containing sections and their keys which are required in a valid configuration file.
     *
     * @var array
     */
    private $_requiredKeys = array(
        'authentication' => array(
            'login',
            'password'
        )
    );


    /**
     * Constructor.
     * 
     * @param Credentials The Digital Measures credentials object to manage.
     */
    public function __construct (&$credentials)
    {
        $this->_credentials = $credentials;
    }

    /**
     * Loads the system configuration from the given config file.
     *
     * @param  string $configFilePath File path to the configuration file which is to be loaded.
     */
    public function loadConfigFile ($configFilePath)
    {
        // Validate path
        $this->_validateConfigFilePath($configFilePath);
        
        // Read
        $newConfig = $this->_readConfigFile($configFilePath);
        $this->filePath = $configFilePath;

        // Validate file
        $this->_validateConfig($newConfig);

        // Save
        $this->_configuration = $newConfig;
        $this->filePath = $configFilePath;

        // Configure system
        $this->_credentials->setLogin($this->_configuration['authentication']['login']);
        $this->_credentials->setPassword($this->_configuration['authentication']['password']);
    }


    /**
     * Validates the given configuration file path.
     * 
     * @param  string $configFilePath The file path to validate.
     *
     * @throws Exception If no file path is provided.
     */
    private function _validateConfigFilePath ($configFilePath)
    {
        // Do we have a file path?
        if ( $configFilePath === '' || ! $configFilePath )
        {
            throw new InvalidArgumentException('No configuration file provided.');
        }
        
        // Is the specified path to an ini file?
        $pathinfo = pathinfo($configFilePath);
        if ( $pathinfo['extension'] !== 'ini' )
        {
            throw new Exception('Invalid configuration file type.');
        }
    }

    /**
     * Reads and parses the given configuration file.
     * 
     * @param  string $configFilePath Path to the configuration file.
     *
     * @throws Exception If configuration file does not exist.
     * @throws Exception If configuration file cannot be read.
     * @throws Exception If configuration file cannot be parsed.
     *
     * @return array Configuration array.
     */
    private function _readConfigFile ($configFilePath)
    {
        // Does the file exist?
        if ( ! file_exists($configFilePath) )
        {
            throw new InvalidArgumentException("Unable to find configuration file '{$configFilePath}'.");
        }

        // Can we read the file?
        if ( ! is_readable($configFilePath) )
        {
            throw new Exception("Unable to read configuration file '{$configFilePath}'.");
        }

        // Parse the config file
        $config = parse_ini_file($configFilePath, TRUE); // TRUE flag parses sections
        if ( $config === FALSE )
        {
            throw new Exception("Unable to parse configuration file '{$configFilePath}'.");
        }

        return $config;
    }

    /**
     * Validates the contents of a given configuration array.
     *
     * @param  array $config Parsed configuration array to be validated.
     *
     * @throws Exception If a section which is required is not found in the given configuration.
     * @throws Exception If a key which is required is not found in the given configuration.
     */
    private function _validateConfig ($config)
    {
        foreach ($this->_requiredKeys as $requiredSection => $requiredSectionKeys)
        {
            if ( ! array_key_exists($requiredSection, $config) )
            {
                throw new Exception ("Section '{$requiredSection}' not found in configuration file.");
            }

            foreach ($requiredSectionKeys as $requiredKey)
            {
                if ( ! array_key_exists($requiredKey, $config[$requiredSection]) )
                {
                    throw new Exception ("Key '{$requiredKey}' not found in section '{$requiredSection}' in configuration file.");
                }
            }
        }
    }

}
?>