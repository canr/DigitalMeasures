<?php
namespace DigitalMeasures;

use \DigitalMeasures\Digital_measures_api;


/**
 * Tests for the Digital Measures API class.
 */

class Digital_measures_apiTest extends \PHPUnit_Framework_TestCase
{
    const defaultConfigFilePath = '/../../config.ini';
    const testConfigFilePath = '/fixtures/testConfig.ini';
    const tempFilesDirPath = '/tempFilesDir';

    public function setUp ()
    {
        $this->setUpTempFilesDir();
    }

    public function tearDown ()
    {
        $this->tearDownTempFilesDir();
    }

    public function testConstructWithDefaultConfigFile ()
    {
        // Ensure that a default config file exists
        $this->stashConfigFile();
        copy(
            dirname(__FILE__) . self::testConfigFilePath,
            dirname(__FILE__) . self::defaultConfigFilePath
        );

        // Construct
        $dm = new Digital_measures_api();

        // Verify file path
        $this->assertSame( $dm->config->filePath, realpath(dirname(__FILE__).self::defaultConfigFilePath) );

        // Tear down copied config
        unlink( realpath( dirname(__FILE__).self::defaultConfigFilePath ) );
        $this->replaceConfigFile();
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage No configuration file found.
     */
    public function testConstructWithDefaultConfigFileAbsent ()
    {
        // Ensure that the default config file does not exist
        $this->stashConfigFile();

        // Construct
        try
        {
            $dm = new Digital_measures_api();
            $this->replaceConfigFile();
        }
        catch (\Exception $exception)
        {
            $this->replaceConfigFile();
            throw $exception;
        }
    }

    public function testConstructWithSpecifiedConfigFile ()
    {
        // Construct with config file fixture
        $dm = new Digital_measures_api( realpath(dirname(__FILE__).'/fixtures/testConfig.ini') );

        // Check that login matches login from fixture
        $this->assertSame( $dm->credentials->getLogin(), 'test/login');
    }

    public function testGetSetUser ()
    {
        $dm = new Digital_measures_api();

        // Initially set to empty string
        $this->assertSame( $dm->get_user(), '' );

        // Set to some arbitrary username
        $test_username = 'test';
        $dm->set_user($test_username);
        $this->assertSame( $dm->get_user(), $test_username);
    }

    public function testGet ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithUnauthorizedCredentials ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithNetworkError ()
    {
        $this->markTestIncomplete();
    }

    public function testGetNonexistantEntity ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithMalformedXpath ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithCacheHit ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithCurlError ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithDigitalMeasuresError ()
    {
        $this->markTestIncomplete();
    }

    public function testGetWithHttpError ()
    {
        $this->markTestIncomplete();
    }

    public function testGetEntitiesList ()
    {
        $this->markTestIncomplete();
    }


    private function setUpTempFilesDir ()
    {
        if ( ! file_exists( dirname(__FILE__).self::tempFilesDirPath ) )
        {
            mkdir(dirname(__FILE__).self::tempFilesDirPath);
        }
    }

    private function tearDownTempFilesDir ()
    {
        if (
            file_exists( dirname(__FILE__).self::tempFilesDirPath )
            && is_dir( dirname(__FILE__).self::tempFilesDirPath ) 
            && count( glob( dirname(__FILE__).self::tempFilesDirPath . '/*' ) ) === 0
        )
        {
            rmdir(dirname(__FILE__).self::tempFilesDirPath);
        }
    }

    private function stashConfigFile ()
    {
        if ( file_exists( realpath(dirname(__FILE__).self::defaultConfigFilePath) ) )
        {
            rename(
                dirname(__FILE__).self::defaultConfigFilePath,
                dirname(__FILE__) . self::tempFilesDirPath . '/config.ini'
            );
        }
    }

    private function replaceConfigFile ()
    {
        if ( file_exists( dirname(__FILE__) . self::tempFilesDirPath . '/config.ini' ) )
        {
            rename(
                dirname(__FILE__) . self::tempFilesDirPath . '/config.ini',
                dirname(__FILE__).self::defaultConfigFilePath
            );
        }
    }
}
?>