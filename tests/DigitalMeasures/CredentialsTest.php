<?php
namespace DigitalMeasures;

use \DigitalMeasures\Credentials;

class CredentialsTest extends \PHPUnit_Framework_TestCase
{
    public function testGetSetLogin ()
    {
        $credentials = new Credentials();

        $this->assertSame( $credentials->getLogin(), '' );

        $credentials->setLogin('testLogin');
        $this->assertSame( $credentials->getLogin(), 'testLogin' );
    }

    public function testGetSetPassword()
    {
        $credentials = new Credentials();

        $this->assertSame( $credentials->getLogin(), '' );

        $credentials->setLogin('testLogin');
        $this->assertSame( $credentials->getLogin(), 'testLogin' );
    }
}
?>