<?php
namespace DigitalMeasures;

use \DigitalMeasures\ConfigurationManager;
use \DigitalMeasures\Credentials;

class ConfigurationManagerTest extends \PHPUnit_Framework_TestCase
{
    const testConfigPath = '/fixtures/testConfig.ini';
    const wrongFileTypeConfigPath = '/fixtures/wrongFileTypeConfig.txt';
    const unreadableConfigPath = '/fixtures/unreadableConfig.ini';
    const unparseableConfigPath = '/fixtures/unparseableConfig.ini';
    const missingSectionConfigPath = '/fixtures/missingSectionConfig.ini';
    const missingKeyConfigPath = '/fixtures/missingKeyConfig.ini';

    public function setUp ()
    {
        $this->credentials = new Credentials();
    }

    public function testLoadConfigFile ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile( dirname(__FILE__) . self::testConfigPath );

        $this->assertSame( $this->credentials->getLogin(), 'test/login');
        $this->assertSame( $this->credentials->getPassword(), 'testpass');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage No configuration file provided.
     */
    public function testLoadConfigFileWithInvalidPath ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile('');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Unable to find configuration file 'fixtures/aFileNameThatShouldNotExist.ini'.
     */
    public function testLoadConfigFileWithMissingFile ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile( 'fixtures/aFileNameThatShouldNotExist.ini' );
    }

    /**
     * @expectedException Exception 
     * @expectedExceptionMessage Invalid configuration file type.
     */
    public function testLoadConfigFileWithWrongFileType ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile( self::wrongFileTypeConfigPath );
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessageRegExp /Unable to read configuration file '.+'\./
     */
    public function testLoadConfigFileWithUnreadableFile ()
    {
        $this->markTestIncomplete('Need to figure out how to make the file unreadable.');

        $configManager = new ConfigurationManager($this->credentials);
        echo "\n" . dirname(__FILE__) . self::unreadableConfigPath . ": " . substr( sprintf( '%o', fileperms( dirname(__FILE__) . self::unreadableConfigPath) ), -4 ) . "\n";
        $flag = chmod( dirname(__FILE__) . self::unreadableConfigPath , 0000 );
        echo "\n" . dirname(__FILE__) . self::unreadableConfigPath . ": " . substr( sprintf( '%o', fileperms( dirname(__FILE__) . self::unreadableConfigPath) ), -4 ) . "\n";
        echo "\n" . (string)$flag . "\n";

        try
        {
            $configManager->loadConfigFile( dirname(__FILE__) . self::unreadableConfigPath );
            chmod( dirname(__FILE__) . self::unreadableConfigPath , 0644 );
        }
        catch (Exception $exception)
        {
            chmod( dirname(__FILE__) . self::unreadableConfigPath , 0644 );
            throw $exception;
        }
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Section 'authentication' not found in configuration file.
     */
    public function testLoadConfigFileWithoutRequiredSection ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile( dirname(__FILE__) . self::missingSectionConfigPath );
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Key 'login' not found in section 'authentication' in configuration file.
     */
    public function testLoadConfigFileWihoutRequiredKey ()
    {
        $configManager = new ConfigurationManager($this->credentials);

        $configManager->loadConfigFile( dirname(__FILE__) . self::missingKeyConfigPath );
    }
}
?>