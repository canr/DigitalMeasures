<?php
// Load and use DM library
require realpath(dirname(__FILE__).'/../vendor/autoload.php');
use \DigitalMeasures\Digital_measures_api;

// Create API object
$dm = new Digital_measures_api();

// Set the user whose data is to be queried
$dm->set_user('campa');

// Get the user's intellectual content data
$publications = $dm->get('//INTELLCONT');

// Get the first publication's title
echo "First publication: \"{$publications[0]->TITLE}\"\n";

// Get each publication's title and authors
foreach ($publications as $publication)
{
    $author_names = array();
    foreach ($publication->INTELLCONT_AUTH as $author)
    {
        $author_names[] = "{$author->FNAME} {$author->MNAME} {$author->LNAME}";
    }
    echo "\"{$publication->TITLE}\" by " . implode(', ', $author_names) . ".\n";
}
?>