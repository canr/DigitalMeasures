<?php
// Load and use DM library
require realpath(dirname(__FILE__).'/../vendor/autoload.php');
use \DigitalMeasures\Digital_measures_api;

// Create API object
$dm = new Digital_measures_api();

// Set the user whose data is to be queried
$dm->set_user($argv[1]);

// Get the entities available
$entities = $dm->get_entities_list();

// Output entities
foreach ($entities as $entity)
{
    echo "{$entity}\n";
}
?>